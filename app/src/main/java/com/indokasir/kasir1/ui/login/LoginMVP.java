package com.indokasir.kasir1.ui.login;


public class LoginMVP {

    public interface View {

        void showLoading(boolean isLoading);

        void showError(String message);

        void onSucses(String id, String email, String username);

    }


    public interface Presenter {
        void login(String email, String password);

        void onDestroy();
    }

}
