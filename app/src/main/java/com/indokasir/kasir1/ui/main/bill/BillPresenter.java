package com.indokasir.kasir1.ui.main.bill;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.util.Log;

import com.indokasir.kasir1.data.Repository;
import com.indokasir.kasir1.data.model.customer.CustomerResponse;
import com.indokasir.kasir1.data.model.payment.PaymentResponse;
import com.indokasir.kasir1.data.model.product.ProductItem;
import com.indokasir.kasir1.data.network.APIRequest;
import com.indokasir.kasir1.utils.BluetoothUtil;
import com.indokasir.kasir1.utils.CurrencyHelper;
import com.indokasir.kasir1.utils.DateFormat;
import com.indokasir.kasir1.utils.PrintUtil;

import java.io.IOException;
import java.util.List;

/**
 * Created by silva on 2/21/2019.
 */

public class BillPresenter implements BillMVP.Presenter {
    private Repository repository;
    private BillMVP.View view;
    private int totalPayment;
    String TAG = "billl";
    private BluetoothSocket bluetoothSocket;

    BillPresenter(Repository repository, BillMVP.View view) {
        this.repository = repository;
        this.view = view;

        if (bluetoothSocket==null){
            bluetoothSocket=BluetoothUtil.getSocket();
        }
    }

    @Override
    public void getListCustomer() {
        repository.getCustomer(new CustomerResponse.CustomerResponseCallback() {
            @Override
            public void onSuccess(CustomerResponse customerResponse) {
                view.showLoading(false);
                view.updateListCustomer(customerResponse.getCustomers());
            }

            @Override
            public void onError(String message, APIRequest.ErrorCode errorCode) {
            }
        });
    }

    @Override
    public void refreshPayment() {
        if (repository.getCustomer() != null){

        }
        if (repository.getProductItems() != null) {
            view.updateListItems(repository.getProductItems());
            totalPayment = 0;
            for (int i = 0; i < repository.getProductItems().size(); i++) {
                totalPayment += repository.getProductItems().get(i).getQty() * repository.getProductItems().get(i).getPrice();

            }
            view.updatePayment(totalPayment);
        }
    }

    @Override
    public void deleteItem(int i) {
        List<ProductItem> productItems = repository.getProductItems();
        totalPayment -= productItems.get(i).getQty() * productItems.get(i).getPrice();
        productItems.remove(i);
        repository.setProductItems(productItems);
        view.updatePayment(totalPayment);

    }

    @Override
    public void edirItem(int i, int qt) {
        List<ProductItem> productItems = repository.getProductItems();
        ProductItem item = productItems.get(i);
        totalPayment -= item.getQty() * item.getPrice();
        item.setQty(qt);
        totalPayment += item.getQty() * item.getPrice();
        productItems.set(i, item);
        repository.setProductItems(productItems);
        view.updatePayment(totalPayment);
        System.out.println();
    }

    @Override
    public void deleteAllItem() {

        List<ProductItem> productItems = repository.getProductItems();
        productItems.clear();
//        for (int i = 0; i < productItems.size(); i++) {
//            productItems.remove(0);
//        }
        repository.setProductItems(productItems);
        refreshPayment();
    }

    @Override
    public void onDestroy() {
        repository.setProductItems(null);
        view = null;
    }

    @Override
    public void onPostPayment() {
        if (repository.getProductItems() != null) {
            view.showLoading(false);
            String products_id = "";
            String products_name = "";
            String products_code = "";
            String quantity = "";
            String price = "";
            String cost = "";
            for (ProductItem productItem : repository.getProductItems()) {
                products_id += productItem.getId() + ";";
                products_name += productItem.getName() + ";";
                products_code += productItem.getCode() + ";";
                quantity += productItem.getQty() + ";";
                price += productItem.getPrice() + ";";
                cost += productItem.getCost() + ";";
            }
            if (bluetoothSocket==null){
                bluetoothSocket=BluetoothUtil.getSocket();
            }else {
                print(bluetoothSocket);

            }
            repository.postPayment(totalPayment, repository.getCustomer(), products_id, products_name, products_code, quantity, price, cost, new PaymentResponse.PaymentResponseCallback() {
                @Override
                public void onSuccess() {
                    view.showLoading(false);
                    view.onPaymentSuccess();


                }

                @Override
                public void onError(String message, APIRequest.ErrorCode errorCode) {
                    Log.e("hasil", "onError: " + message);
                    view.showLoading(false);
                    view.onPaymentError(message);
                }
            });
        }
        view.showLoading(false);
    }




    private void print(BluetoothSocket bluetoothSocket) {
        try {
            PrintUtil pUtil = new PrintUtil(bluetoothSocket.getOutputStream(), "GBK");
            // Nama Toko di Tengah Zoom
            pUtil.printAlignment(1);
            pUtil.printLargeText("INVOICE");
            pUtil.printLine();
            pUtil.printAlignment(0);
            pUtil.printLine();

            pUtil.printTwoColumn("Date:", DateFormat.getDateNow());
            pUtil.printLine();

            pUtil.printTwoColumn("Customer:", repository.getCustomer().getName());
            pUtil.printLine();

            pUtil.printTwoColumn("Note:", "");
            pUtil.printLine();

            pUtil.printDashLine();

            for (ProductItem productItem : repository.getProductItems()) {
                pUtil.printText(productItem.getName());
                pUtil.printLine();

                pUtil.printTwoColumn(CurrencyHelper.formatRP(productItem.getPrice())+ " x " +
                                String.valueOf(productItem.getQty()),
                        "= " + CurrencyHelper.formatRP(productItem.getPrice() * productItem.getQty()));
                pUtil.printLine();
            }
            pUtil.printDashLine();

            pUtil.printTwoColumn("Total Items:", String.valueOf(repository.getProductItems().size()));
            pUtil.printLine();

            pUtil.printTwoColumn("Total:", CurrencyHelper.formatRP(totalPayment));
            pUtil.printLine();
            pUtil.printTwoColumn("Paid:", CurrencyHelper.formatRP(totalPayment));
            pUtil.printLine();

            pUtil.printTwoColumn("Return:", "0");
            pUtil.printLine();
            pUtil.printLine();

            pUtil.printTwoColumn("User:", repository.getUserName());
            pUtil.printLine();


            //    pUtil.printBitmap(bitmap);
            pUtil.printLine(4);

        } catch (IOException e) {

        }
    }

    @Override
    public void onPostSuspend(String note) {
        if (repository.getProductItems() != null) {
            view.showLoading(false);
            String products_id = "";
            String products_name = "";
            String products_code = "";
            String quantity = "";
            String price = "";
            String cost = "";
            for (ProductItem productItem : repository.getProductItems()) {
                products_id += productItem.getId() + ";";
                products_name += productItem.getName() + ";";
                products_code += productItem.getCode() + ";";
                quantity += productItem.getQty() + ";";
                price += productItem.getPrice() + ";";
                cost += productItem.getCost() + ";";
            }
            repository.postSuspend(totalPayment, repository.getCustomer(), note, products_id, products_name, products_code, quantity, price, cost, new PaymentResponse.PaymentResponseCallback() {
                @Override
                public void onSuccess() {
                    view.showLoading(false);
                    view.onPaymentSuccess();
                }

                @Override
                public void onError(String message, APIRequest.ErrorCode errorCode) {
                    Log.e("hasil", "onError: " + message);
                    view.showLoading(false);
                    view.onPaymentError(message);
                }
            });
        }
        view.showLoading(false);
    }
}
