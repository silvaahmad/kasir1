package com.indokasir.kasir1.ui.splashscreen;

import android.content.Intent;
import android.os.Bundle;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.base.BaseActivity;
import com.indokasir.kasir1.data.Repository;
import com.indokasir.kasir1.ui.main.MainActivity;
import com.indokasir.kasir1.ui.login.LoginActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Repository repository = new Repository(this);
        if (repository.getUserId() != null)
            startActivity(new Intent(this, MainActivity.class));
        else
            startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void setUp() {

    }
}
