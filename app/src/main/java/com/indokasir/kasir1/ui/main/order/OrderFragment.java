package com.indokasir.kasir1.ui.main.order;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.base.BaseFragment;
import com.indokasir.kasir1.data.model.product.ProductItem;
import com.indokasir.kasir1.utils.CallbackItemView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderFragment extends BaseFragment implements OrderMVP.View {

    @BindView(R.id.rvOrder)
    RecyclerView rvView;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.etSearch)
    EditText etSearch;

    private OrderAdapter adapter;
    private OrderPresenter presenter;

    private String TAG = "order";

    public OrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order, container, false);

        Unbinder unbinder = ButterKnife.bind(this, view);
        setUnbinder(unbinder);

        presenter = new OrderPresenter(getRepository(), this);
        presenter.loadData();

        initRv();
        onClick();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.updateData();
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (presenter != null)
                presenter.updateData();
        }
    }

    @Override
    public void showLoading(boolean isLoading) {
        if (isLoading) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
        System.out.println();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void upDateList(List<ProductItem> ProductItem) {
        adapter.updateData(ProductItem);
    }

    private void initRv() {

        adapter = new OrderAdapter(getContext());
        rvView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        rvView.setAdapter(adapter);
        rvView.setNestedScrollingEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadData();
            }
        });

        adapter.setCallback(new CallbackItemView() {
            @Override
            public void onItemClick(int position, View view) {
                ProductItem productItem = adapter.getItem(position);
//                viewPager.setCurrentItem(1, false);
                presenter.addOrder(productItem);

                Log.e(TAG, "onItemClick: ");
            }
        });
    }

    private void onClick() {
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                presenter.search(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
