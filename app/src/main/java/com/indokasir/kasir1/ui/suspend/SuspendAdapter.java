package com.indokasir.kasir1.ui.suspend;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.data.model.suspend.SuspendItem;
import com.indokasir.kasir1.utils.CallbackItemView;
import com.indokasir.kasir1.utils.CurrencyHelper;

import java.util.ArrayList;
import java.util.List;

public class SuspendAdapter extends RecyclerView.Adapter<SuspendAdapter.Holder> {

    private List<SuspendItem> data;
    private CallbackItemView callback;
    private Context context;

    public SuspendAdapter(Context context) {
        data = new ArrayList<>();
        this.context = context;
        System.out.println();
    }

    public void updateData(List<SuspendItem> dataList) {
        data.clear();
        data.addAll(dataList);
        notifyDataSetChanged();
    }

    public List<SuspendItem> getData() {
        return data;
    }

    public void setCallback(CallbackItemView callback) {
        this.callback = callback;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_suspend, viewGroup, false);
        final Holder holder = new Holder(view);
        view.getId();
        if (callback != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(holder.getPosition(), view);
                }
            });
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.tvDate.setText(data.get(i).getDate());
        holder.tvCustomerName.setText(data.get(i).getCustomer_name());
        holder.tvNote.setText(data.get(i).getHold_ref());
        holder.tvTotalItems.setText(data.get(i).getTotal_items() + "");
        holder.tvGrandTotal.setText(CurrencyHelper.formatRP(data.get(i).getGrand_total()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.line.setBackgroundColor(i % 2 == 0 ? context.getColor(R.color.colorBG2) : context.getColor(R.color.colorBG));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class Holder extends RecyclerView.ViewHolder {

        TextView tvDate, tvCustomerName, tvNote, tvTotalItems, tvGrandTotal;
        LinearLayout line;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvCustomerName = itemView.findViewById(R.id.tvCustomerName);
            tvNote = itemView.findViewById(R.id.tvNote);
            tvGrandTotal = itemView.findViewById(R.id.tvGrandTotal);
            tvTotalItems = itemView.findViewById(R.id.tvTotalItem);
            line = itemView.findViewById(R.id.line);
        }
    }

}

