package com.indokasir.kasir1.ui.suspend;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.base.BaseActivity;
import com.indokasir.kasir1.data.model.suspend.SuspendItem;
import com.indokasir.kasir1.utils.CallbackItemView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by User on 3/12/2019.
 */

public class SuspendActivity extends BaseActivity implements SuspendMVP.View{


    @BindView(R.id.rvSuspend)
    RecyclerView rvView;
    @BindView(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private Dialog loading;
    private SuspendPresenter presenter;
    private SuspendAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspend);
        Unbinder unbinder = ButterKnife.bind(this);
        setUnbinder(unbinder);

        presenter = new SuspendPresenter(getRepository(), this);
        presenter.loadData();

        setUp();

    }


    @Override
    public void setUp() {
        adapter = new SuspendAdapter(this);
        rvView.setLayoutManager(new LinearLayoutManager(this));
        rvView.setAdapter(adapter);

        adapter.setCallback(new CallbackItemView() {
            @Override
            public void onItemClick(int position, View view) {
                presenter.resetSuspend(adapter.getData().get(position));
            }
        });
    }

    @Override
    public void showLoading(boolean isLoading) {

        if (isLoading) {
            swipeRefreshLayout.setRefreshing(true);
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showError(String message) {

    }


    @Override
    public void upDateList(List<SuspendItem> suspendItems) {
        adapter.updateData(suspendItems);
    }

    @Override
    public void onFinish() {
        finish();

    }


}
