package com.indokasir.kasir1.ui.main.bill;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.data.model.product.ProductItem;
import com.indokasir.kasir1.utils.CallbackItemView;
import com.indokasir.kasir1.utils.CurrencyHelper;

import java.util.ArrayList;
import java.util.List;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.Holder> {

    private List<ProductItem> data;
    private CallbackItemView callback;
    private Context context;

    public BillAdapter(Context context) {
        data = new ArrayList<>();
        this.context = context;
        System.out.println();
    }

    public void updateData(List<ProductItem> dataList) {
        data.clear();
        data.addAll(dataList);
        notifyDataSetChanged();
    }

    public void setCallback(CallbackItemView callback) {
        this.callback = callback;
    }

    public ProductItem getItem(int position) {
        return data.get(position);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_bill, viewGroup, false);
        final Holder holder = new Holder(view);
        view.getId();
        if (callback != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(holder.getPosition(), view);
                }
            });
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.tvName.setText(data.get(i).getName());
        holder.tvQty.setText(data.get(i).getQty() + "");
        holder.tvPrice.setText(CurrencyHelper.formatRP(data.get(i).getPrice()));
        holder.tvSubTotal.setText(CurrencyHelper.formatRP(data.get(i).getPrice() * data.get(i).getQty()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.line.setBackgroundColor(i % 2 == 0 ? context.getColor( R.color.colorBG2) : context.getColor(R.color.colorBG));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class Holder extends RecyclerView.ViewHolder {

        TextView tvName, tvQty, tvPrice, tvSubTotal;
        LinearLayout line;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvProduct);
            tvQty = itemView.findViewById(R.id.tvQty);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvSubTotal = itemView.findViewById(R.id.tvSubTotal);
            line = itemView.findViewById(R.id.line);
        }
    }

}

