package com.indokasir.kasir1.ui.main.bill;

import com.indokasir.kasir1.data.model.customer.Customer;
import com.indokasir.kasir1.data.model.product.ProductItem;

import java.util.List;

/**
 * Created by silva on 2/21/2019.
 */

public class BillMVP {

    public interface View {
        void updateListCustomer(List<Customer> customers);

        void updateListItems(List<ProductItem> ProductItem);

        void updatePayment(int value);

        void onPaymentSuccess();

        void onPaymentError(String message);

        void showLoading(boolean isLoading);
    }


    public interface Presenter {
        void getListCustomer();

        void refreshPayment();

        void deleteItem(int i);

        void edirItem(int i, int qt);

        void deleteAllItem();

        void onDestroy();

        void onPostPayment();

        void onPostSuspend(String note);
    }

}
