package com.indokasir.kasir1.ui.main.order;


import android.util.Log;

import com.indokasir.kasir1.data.Repository;
import com.indokasir.kasir1.data.model.customer.Customer;
import com.indokasir.kasir1.data.model.product.ProductItem;
import com.indokasir.kasir1.data.model.product.ProductResponse;
import com.indokasir.kasir1.data.model.suspend.Items;
import com.indokasir.kasir1.data.network.APIRequest;

import java.util.ArrayList;
import java.util.List;

public class OrderPresenter implements OrderMVP.Presenter {

    static String TAG = "orderr";

    private Repository repository;
    private OrderMVP.View view;
    private ArrayList<ProductItem> productItemsRepo;
    private List<ProductItem> productItemsOrder;
    private List<ProductItem> productItemsOrderAll;

    public OrderPresenter(Repository repository, OrderMVP.View view) {
        this.repository = repository;
        this.view = view;
        productItemsRepo = new ArrayList<>();
        productItemsOrder = new ArrayList<>();
        productItemsOrderAll = new ArrayList<>();
    }

    @Override
    public void loadData() {
        view.showLoading(true);
        repository.getProducts(new ProductResponse.ProductsResponseCallback() {
            @Override
            public void onSuccess(ProductResponse productItems) {
                Log.e(TAG, "onSuccess: ");
                if (view != null) {
                    view.showLoading(false);
                    productItemsOrder = productItems.getProductItems();
                    productItemsOrderAll = productItems.getProductItems();
                    view.upDateList(productItemsOrder);
                }

            }

            @Override
            public void onError(String message, APIRequest.ErrorCode errorCode) {
                if (view != null) {
                    view.showLoading(false);
                    view.showError("Gagal memuat");
                }

            }
        });

    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void addOrder(ProductItem productItem) {
        setProduct(productItem);
        repository.setProductItems(productItemsRepo);
        updateData();
    }

    @Override
    public void updateData() {
        if (repository.getSuspendItem() != null) {
            List<Items> items = repository.getSuspendItem().getItems();
            if (items.size() > 0) {
                Log.d(TAG, "updateData: suspend");
                Log.d(TAG, "updateData: " + productItemsOrderAll.size());
                List<ProductItem> productItems = repository.getProductItems();
                productItems.clear();
                ProductItem productItemDunp = new ProductItem();
                for (Items item : items) {
                    Log.d(TAG, "updateData: suspend" + item.getProduct_name());
                    for (ProductItem productItem : productItemsOrderAll) {
                        Log.d(TAG, "updateData: " + productItem.getId() + " : " + item.getId());
                        if (productItem.getId() == item.getProductId()) {
                            Log.d(TAG, "updateData: add");
                            productItemDunp = productItem;
                            productItemDunp.setQty(item.getQty());
                            setProduct(productItemDunp);
                        }
                    }
                }
                Customer customer = new Customer();
                customer.setName(repository.getSuspendItem().getCustomer_name());
                repository.setCustomer(customer);
                repository.setProductItems(productItemsRepo);

            }
            repository.setSuspendItem(null);
        }

        for (int i = 0; i < productItemsOrder.size(); i++) {
            ProductItem item1 = productItemsOrder.get(i);
            boolean ordered = false;
            for (ProductItem repositoryItem : repository.getProductItems()) {
                if (repositoryItem.getId() == item1.getId()) {
                    ordered = true;
                }
            }
            item1.setOrdered(ordered);
            productItemsOrder.set(i, item1);
        }
        view.upDateList(productItemsOrder);
    }

    @Override
    public void search(String s) {
        productItemsOrder = new ArrayList<>();
        Log.d(TAG, s + " search: " + productItemsOrderAll.size());
        for (int i = 0; i < productItemsOrderAll.size(); i++) {
            if (productItemsOrderAll.get(i).getName().toLowerCase().contains(s.toLowerCase())) {
                Log.d(TAG, "search: " + productItemsOrderAll.get(i).getName());
                productItemsOrder.add(productItemsOrderAll.get(i));
            }
        }
        Log.d(TAG, "search: ");
        updateData();
//        view.upDateList(productItemsOrder);
    }

    private void setProduct(ProductItem productItem) {
        for (int i = 0; i < productItemsRepo.size(); i++) {
            if (productItemsRepo.get(i).getId() == productItem.getId()) {
                productItem = productItemsRepo.get(i);
                productItem.setQty(productItem.getQty() + 1);
                productItemsRepo.set(i, productItem);
                return;
            }
        }
        if (productItem.getQty() == 0)
            productItem.setQty(1);
        productItemsRepo.add(productItem);
    }

}
