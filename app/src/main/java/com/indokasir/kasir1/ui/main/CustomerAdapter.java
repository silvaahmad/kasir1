package com.indokasir.kasir1.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.data.model.customer.Customer;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by silva on 03/08/17.
 */

public class CustomerAdapter extends BaseAdapter {
    Context context;
    List<Customer> data;
    LayoutInflater inflater;

    public CustomerAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        this.inflater = (LayoutInflater.from(context));
    }

    public void updateData(List<Customer> customers) {
        data = customers;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.layout_customer, null);
        TextView textView = view.findViewById(R.id.customer);
        textView.setText(data.get(i).getName());
        return view;
    }
//
//    // Filter Class
//    public void filter(String charText) {
//        if (tempData.isEmpty())
//            tempData.addAll(data);
//        data.clear();
//        for (id.com.aksamedia.bekas.model.city.City p : tempData) {
//            if (p.getNamaArea().trim().toLowerCase().contains(charText)) {
//                data.add(p);
//            }
//        }
//    }
}
