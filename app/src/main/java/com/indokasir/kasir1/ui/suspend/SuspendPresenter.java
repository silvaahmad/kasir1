package com.indokasir.kasir1.ui.suspend;


import android.util.Log;


import com.indokasir.kasir1.data.Repository;
import com.indokasir.kasir1.data.model.suspend.SuspendItem;
import com.indokasir.kasir1.data.model.suspend.SuspendResponse;
import com.indokasir.kasir1.data.network.APIRequest;

public class SuspendPresenter implements SuspendMVP.Presenter {

    static String TAG = "suspe";

    private Repository repository;
    private SuspendMVP.View view;

    public SuspendPresenter(Repository repository, SuspendMVP.View view) {
        this.repository = repository;
        this.view = view;
//        productItems = new ArrayList<>();
    }

    @Override
    public void loadData() {
        view.showLoading(true);
        repository.getSuspend(new SuspendResponse.SuspendResponseCallback() {
            @Override
            public void onSuccess(SuspendResponse SuspendResponse) {
                Log.e(TAG, "onSuccess: ");
                if (view != null) {
                    view.showLoading(false);
                    view.upDateList(SuspendResponse.getSuspendItems());
                }
            }

            @Override
            public void onError(String message, APIRequest.ErrorCode errorCode) {
                if (view != null) {
                    view.showLoading(false);
                    view.showError("Gagal memuat");
                }
            }
        });


    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void resetSuspend(SuspendItem suspendItem) {
        repository.setSuspendItem(suspendItem);
        view.onFinish();
    }


}
