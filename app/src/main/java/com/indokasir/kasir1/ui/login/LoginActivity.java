package com.indokasir.kasir1.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.base.BaseActivity;
import com.indokasir.kasir1.ui.main.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LoginActivity extends BaseActivity implements LoginMVP.View {


    @BindView(R.id.email)
    EditText textEmail;
    @BindView(R.id.password)
    EditText textpassword;
    @BindView(R.id.login_progress)
    ProgressBar progressBar;
    @BindView(R.id.email_sign_in_button)
    Button button;

    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Unbinder unbinder = ButterKnife.bind(this);
        setUnbinder(unbinder);


        presenter = new LoginPresenter(getRepository(), this);

        setUp();
    }

    @Override
    public void setUp() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.login(textEmail.getText().toString(), textpassword.getText().toString());
            }
        });
    }

    @Override
    public void showLoading(boolean isLoading) {

    }

    @Override
    public void showError(String message) {
//        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Toast.makeText(this, "Email atau passord salah", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSucses(String id, String email, String username) {
        startActivity(new Intent(this, MainActivity.class));
        finish();

    }
}
