package com.indokasir.kasir1.ui.main.bill;


import android.app.Dialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.base.BaseFragment;
import com.indokasir.kasir1.data.model.customer.Customer;
import com.indokasir.kasir1.data.model.product.ProductItem;
import com.indokasir.kasir1.ui.main.CustomerAdapter;
import com.indokasir.kasir1.utils.CallbackItemView;
import com.indokasir.kasir1.utils.CurrencyHelper;
import com.indokasir.kasir1.utils.DateFormat;
import com.indokasir.kasir1.utils.DialogHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class BillFragment extends BaseFragment implements BillMVP.View {

    @BindView(R.id.rvBill)
    RecyclerView rvView;
    @BindView(R.id.btn_payment)
    Button btPayment;
    @BindView(R.id.btn_suspend)
    Button btSuspend;
    @BindView(R.id.tvCustomerName)
    TextView tvCustomer;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.cancel)
    LinearLayout cancel;
    String TAG = "bill";
    private BillPresenter presenter;
    private BillAdapter billAdapter;
    private Dialog dialogPayment;
    private Dialog loading;
    private int totalPayment;
    private List<Customer> customers = new ArrayList<>();
    List<ProductItem> productItems = new ArrayList<>();

    public BillFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bill, container, false);

        Unbinder unbinder = ButterKnife.bind(this, view);
        setUnbinder(unbinder);

        presenter = new BillPresenter(getRepository(), this);

        init();
        onClick();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void init() {

        tvDate.setText(DateFormat.getDateNow());

        billAdapter = new BillAdapter(getContext());
        rvView.setLayoutManager(new LinearLayoutManager(getContext()));
        rvView.setAdapter(billAdapter);

        presenter.getListCustomer();
    }


    private void onClick() {
        btPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPayment = confirmPayment();
                dialogPayment.show();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.deleteAllItem();
            }
        });
        btSuspend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                suspendDialog();
            }
        });
        tvCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCustomer();
            }
        });
        billAdapter.setCallback(new CallbackItemView() {
            @Override
            public void onItemClick(final int position, View view) {
                view.findViewById(R.id.ivDelete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d(TAG, "onItemClick: delete");
                        presenter.deleteItem(position);
                        presenter.refreshPayment();
                    }
                });
                view.findViewById(R.id.ivEdit).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        editItemDialog(position);

                    }
                });
            }
        });


    }

    public Dialog confirmPayment() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_payment);
        final Button btnConfirm = dialog.findViewById(R.id.btn_confirm);
        final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        final TextView tvTotal = dialog.findViewById(R.id.tv_total);
        final TextView tvReturn = dialog.findViewById(R.id.tv_return);
        final EditText edCash = dialog.findViewById(R.id.ed_cash);
        final int total = totalPayment;
        tvTotal.setText(CurrencyHelper.formatRP(totalPayment));
        edCash.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                try {
                    int a = Integer.parseInt(s.toString());
                    int result = total - a;
                    tvReturn.setText(CurrencyHelper.formatRP(result));
                } catch (Exception e) {

                }
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                presenter.onPostPayment();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        return dialog;

    }

    public void editItemDialog(final int i) {
        final ProductItem productItem = productItems.get(i);
        final int[] dqty = {productItem.getQty()};
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_item);
        final Button btnConfirm = dialog.findViewById(R.id.btn_confirm);
        final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        final TextView tvName = dialog.findViewById(R.id.tvName);
        final TextView tvQty = dialog.findViewById(R.id.tvQty);
        final ImageView ivAdd = dialog.findViewById(R.id.ivAdd);
        final ImageView ivRemove = dialog.findViewById(R.id.ivRemove);

        tvName.setText(productItem.getName());
        tvQty.setText(productItem.getQty()+"");
        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dqty[0]++;
                tvQty.setText(String.valueOf(dqty[0]));
                System.out.println();
            }
        });
        ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dqty[0]--;
                tvQty.setText(String.valueOf(dqty[0]));
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productItem.setQty(Integer.parseInt(tvQty.getText().toString()));
                productItems.set(i, productItem);
                presenter.edirItem(i, productItem.getQty());
                presenter.refreshPayment();
                dialog.cancel();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();

    }

    public void suspendDialog() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_note_suspend);
        final Button btnConfirm = dialog.findViewById(R.id.btn_confirm);
        final Button btnCancel = dialog.findViewById(R.id.btn_cancel);
        final EditText edNote = dialog.findViewById(R.id.ed_note);


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onPostSuspend(edNote.getText().toString());
                dialog.cancel();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();

    }

    private void dialogCustomer() {
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_customer);
        ListView list = dialog.findViewById(R.id.list_kota);
        CustomerAdapter customerAdapter = new CustomerAdapter(getContext());
        list.setAdapter(customerAdapter);
        customerAdapter.updateData(customers);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                dialog.dismiss();
                tvCustomer.setText(customers.get(i).getName());
                getRepository().setCustomer(customers.get(i));
            }
        });
        dialog.show();
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            presenter.refreshPayment();
        }
    }

    @Override
    public void updateListCustomer(List<Customer> customers) {
        Log.d(TAG, "updateListCustomer: ");
        tvCustomer.setText(customers.get(0).getName());
        this.customers = customers;
        getRepository().setCustomer(customers.get(0));
    }

    @Override
    public void updateListItems(List<ProductItem> ProductItem) {
        this.productItems =ProductItem;
        billAdapter.updateData(this.productItems);
    }

    @Override
    public void updatePayment(int value) {
        totalPayment = value;
        btPayment.setText(CurrencyHelper.formatRP(totalPayment));
    }

    @Override
    public void onPaymentSuccess() {
        loading.cancel();
        loading.dismiss();
        presenter.deleteAllItem();
        Toast.makeText(getContext(), "Payment Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPaymentError(String message) {
        loading.cancel();
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(boolean isLoading) {
        loading = DialogHelper.loading(getContext());
        if (isLoading)
            loading.show();
        else
            loading.cancel();
    }


    @Override
    public void onDestroyView() {
        presenter.onDestroy();
        super.onDestroyView();
    }

}
