package com.indokasir.kasir1.ui.main.order;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.indokasir.kasir1.R;
import com.indokasir.kasir1.data.model.product.ProductItem;
import com.indokasir.kasir1.utils.CallbackItemView;
import com.indokasir.kasir1.utils.CurrencyHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.Holder> {

    private List<ProductItem> data;
    private CallbackItemView callback;
    private Context context;

    public OrderAdapter(Context context) {
        data = new ArrayList<>();
        this.context = context;
    }

    public void updateData(List<ProductItem> dataList) {
        data.clear();
        data.addAll(dataList);
        notifyDataSetChanged();
    }

    public void setCallback(CallbackItemView callback) {
        this.callback = callback;
    }

    public ProductItem getItem(int position) {
        return data.get(position);
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_orders, viewGroup, false);
        final Holder holder = new Holder(view);
        if (callback != null) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback.onItemClick(holder.getPosition(), view);
                }
            });
            System.out.println();
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.tvName.setText(data.get(i).getName());
        Picasso.with(context)
                .load("http://kasir.org/sp4/uploads/thumbs/"+data.get(i).getImage())
                .placeholder(R.drawable.ic_image)
                .into(holder.ivProduct);
        holder.tvPrice.setText(CurrencyHelper.formatRP(data.get(i).getPrice()));
        holder.ivTag.setVisibility(data.get(i).isOrdered() ? View.VISIBLE : View.GONE);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class Holder extends RecyclerView.ViewHolder {

        TextView tvName, tvPrice;
        ImageView ivProduct, ivTag;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvProduct);
            ivTag = itemView.findViewById(R.id.ivTag);
            tvPrice =  itemView.findViewById(R.id.tvPrice);
            ivProduct = itemView.findViewById(R.id.ivProduct);
        }
    }

}

