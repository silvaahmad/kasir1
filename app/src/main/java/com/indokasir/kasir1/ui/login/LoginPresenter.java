package com.indokasir.kasir1.ui.login;


import com.indokasir.kasir1.data.Repository;
import com.indokasir.kasir1.data.model.login.LoginResponse;
import com.indokasir.kasir1.data.network.APIRequest;

public class LoginPresenter implements LoginMVP.Presenter {

    static String TAG = "suspe";

    private Repository repository;
    private LoginMVP.View view;

    public LoginPresenter(Repository repository, LoginMVP.View view) {
        this.repository = repository;
        this.view = view;
//        productItems = new ArrayList<>();
    }


    @Override
    public void onDestroy() {
        view = null;
    }


    @Override
    public void login(String email, final String password) {
        repository.getLogin(email, password, new LoginResponse.ResponseCallback() {
            @Override
            public void onSuccess(String id, String email, String username) {
                view.onSucses(id, email, username);
                repository.saveUser(id, email, username);
            }

            @Override
            public void onError(String message, APIRequest.ErrorCode errorCode) {
                view.showError(message);
            }
        });
    }
}
