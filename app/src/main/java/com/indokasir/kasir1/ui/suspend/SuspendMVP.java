package com.indokasir.kasir1.ui.suspend;


import com.indokasir.kasir1.data.model.suspend.SuspendItem;

import java.util.List;

public class SuspendMVP {

    public interface View {

        void showLoading(boolean isLoading);

        void showError(String message);

        void upDateList(List<SuspendItem> suspendItems);

        void onFinish();

    }


    public interface Presenter {
        void loadData();

        void onDestroy();

        void resetSuspend(SuspendItem suspendItem);
    }

}
