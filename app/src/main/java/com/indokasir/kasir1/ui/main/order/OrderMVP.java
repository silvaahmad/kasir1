package com.indokasir.kasir1.ui.main.order;


import com.indokasir.kasir1.data.model.product.ProductItem;

import java.util.List;

public class OrderMVP {

    public interface View {
        void showLoading(boolean isLoading);

        void showError(String message);

        void upDateList(List<ProductItem> ProductItem);
    }


    public interface Presenter {
        void loadData();

        void onDestroy();

        void addOrder(ProductItem productItem);

        void updateData();

        void search(String s);
    }

}
