package com.indokasir.kasir1.utils;


import android.view.View;


public interface CallbackItemView {
    void onItemClick(int position, View view);
}


