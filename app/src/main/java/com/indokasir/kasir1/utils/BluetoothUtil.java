package com.indokasir.kasir1.utils;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class BluetoothUtil {

    // android built in classes for bluetooth operations
    static BluetoothAdapter mBluetoothAdapter;

    static BluetoothSocket mmSocket;
    static BluetoothDevice mmDevice;

    // needed for communication to bluetooth device / network
    static OutputStream mmOutputStream;
    static InputStream mmInputStream;
    static Thread workerThread;

    static byte[] readBuffer;
    static int readBufferPosition;
    static volatile boolean stopWorker;
    static String TAG = "btbtbt";

    BluetoothUtil() {

    }

    public static BluetoothSocket getSocket()  {
        findBT(new CallBackFindBluetooh() {
            @Override
            public void found() {

            }

            @Override
            public void notAviable() {

            }

            @Override
            public void notEnabble(Intent enableBluetooth) {

            }
        });
        try {
            openBT(new CallBAckOpenBT() {
                @Override
                public void opened(BluetoothSocket bluetoothSocket) {

                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  mmSocket;
    }
    // this will find a bluetooth printer device
    public static void findBT(CallBackFindBluetooh callBackFindBluetooh) {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
//                myLabel.setText("No bluetooth adapter available");
                callBackFindBluetooh.notAviable();
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//                startActivityForResult(enableBluetooth, 0);
                callBackFindBluetooh.notEnabble(enableBluetooth);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    if (device.getName().equals("MPT-II")) {
                        mmDevice = device;
                        break;
                    }
                }
            }

//            myLabel.setText("Bluetooth device found.");
            callBackFindBluetooh.found();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isBluetoothOn() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null)
            // Bluetooth aktif
            if (mBluetoothAdapter.isEnabled())
                return true;

        return false;
    }

    // tries to open a connection to the bluetooth printer device
    public static void openBT(CallBAckOpenBT callBAckOpenBT) throws IOException {
        try {

            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();
            mmInputStream = mmSocket.getInputStream();

            beginListenForData();

//            myLabel.setText("Bluetooth Opened");
            callBAckOpenBT.opened(mmSocket);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "openBT: " + e.getMessage());

            callBAckOpenBT.opened(mmSocket);
        }
    }

    static void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
//                                                myLabel.setText(data);
                                            }
                                        });

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // this will send text data to be printed by the bluetooth printer
    void sendData(String msg, CallBackSendData callBackSendData) throws IOException {
        try {

            // the text typed by the user
//            String msg = myTextbox.getText().toString();
            msg += "\n";

            mmOutputStream.write(msg.getBytes());

            PrintUtil.printTest(mmSocket, null);
            // tell the user data were sent
//            myLabel.setText("Data sent.");
            callBackSendData.sended();

        } catch (Exception e) {
            e.printStackTrace();
        }
//        Bitmap anImage      = getBitmapFromVectorDrawable(this, R.drawable.ic_price_tag);
//        printBitmap(anImage);

    }

    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
//            myLabel.setText("Bluetooth Closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface CallBackFindBluetooh {
        void found();

        void notAviable();

        void notEnabble(Intent enableBluetooth);
    }

    public interface CallBAckOpenBT {
        void opened(BluetoothSocket bluetoothSocket);
    }

    public interface CallBackSendData {
        void sended();
    }

    public interface CallBAckClosedBT {
        void closed();
    }


}