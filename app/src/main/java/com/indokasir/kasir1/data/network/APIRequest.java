package com.indokasir.kasir1.data.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.indokasir.kasir1.data.model.customer.Customer;
import com.indokasir.kasir1.data.model.customer.CustomerResponse;
import com.indokasir.kasir1.data.model.login.LoginResponse;
import com.indokasir.kasir1.data.model.payment.PaymentResponse;
import com.indokasir.kasir1.data.model.product.ProductResponse;
import com.indokasir.kasir1.data.model.suspend.SuspendResponse;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by User on 11/26/2018.
 */

public class APIRequest {

    private final String BASE_URL = "http://kasir.org/restserver/api/";
    private String TAG = "hasil";
    private AsyncHttpClient client;
    private Gson gson;

    public APIRequest() {
        gson = new GsonBuilder().create();
        client = new AsyncHttpClient();
//        client.addHeader("key","");
    }

    public void getProducts(final ProductResponse.ProductsResponseCallback callback) {
        String requestUrl = BASE_URL + "products";
        client.get(requestUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String result = null;
                JSONObject response;
                try {
                    response = new JSONObject(new String(responseBody));
                    result = response.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onSuccess: " + e.getMessage());
                }
                Log.d(TAG, "onSuccess: " + result);

                ProductResponse productResponse = gson.fromJson(result, ProductResponse.class);

                if (productResponse != null) {
                    callback.onSuccess(productResponse);
                } else {
                    callback.onError("Cannot get Object", ErrorCode.NOT_FOUND);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onError("Koneksi gagal", ErrorCode.NO_INTERNET);
            }
        });

    }

    public void getSuspend(final SuspendResponse.SuspendResponseCallback callback) {
        String requestUrl = BASE_URL + "suspend";
        client.get(requestUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String result = null;
                JSONObject response;
                try {
                    response = new JSONObject(new String(responseBody));
                    result = response.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onSuccess: " + e.getMessage());
                }
                Log.d(TAG, "onSuccess: " + result);

                SuspendResponse suspendResponse = gson.fromJson(result, SuspendResponse.class);

                if (suspendResponse != null) {
                    callback.onSuccess(suspendResponse);
                } else {
                    callback.onError("Cannot get Object", ErrorCode.NOT_FOUND);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onError("Koneksi gagal", ErrorCode.NO_INTERNET);
            }
        });

    }

    public void getCustomer(final CustomerResponse.CustomerResponseCallback callback) {
        String requestUrl = BASE_URL + "customers";
        client.get(requestUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String result = null;
                JSONObject response;
                try {
                    response = new JSONObject(new String(responseBody));
                    result = response.toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onSuccess: " + e.getMessage());
                }
                Log.d(TAG, "onSuccess: " + result);

                CustomerResponse customerResponse = gson.fromJson(result, CustomerResponse.class);

                if (customerResponse != null) {
                    callback.onSuccess(customerResponse);
                } else {
                    callback.onError("Cannot get Object", ErrorCode.NOT_FOUND);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d(TAG, "onFailure: " + error.getMessage());
                callback.onError("Koneksi gagal", ErrorCode.NO_INTERNET);
            }
        });
    }

    public void getLogin(String email, String password,
                         final LoginResponse.ResponseCallback callback) {
        String requestUrl = BASE_URL + "/login";
        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("password", password);
        params.setUseJsonStreamer(true);

        Log.d(TAG, "onPostPayment: " + requestUrl);

        client.post(requestUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d(TAG, "onSuccess: " + new String(responseBody));
                String respon = new String(responseBody);
                try {
                    JSONObject object = new JSONObject(respon);
                    callback.onSuccess(object.getString("id"),
                            object.getString("email"), object.getString("username"));
                } catch (JSONException e) {
                    callback.onError(e.getMessage(), ErrorCode.SERVER_ERROR);
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onError(new String(responseBody), ErrorCode.NO_INTERNET);
            }
        });
    }

    public void postPayment(int total, Customer customer, String products_id, String products_name, String products_code, String quantity, String price, String cost,
                            final PaymentResponse.PaymentResponseCallback callback) {
        String requestUrl = BASE_URL + "/insertpos";
        RequestParams params = new RequestParams();
        params.put("total", total);
        params.put("customer_id", customer.getId());
        params.put("customer_name", customer.getName());
        params.put("products_id", products_id);
        params.put("products_name", products_name);
        params.put("products_code", products_code);
        params.put("price", price);
        params.put("quantity", quantity);
        params.put("cost", cost);
        Log.d(TAG, "postPayment: " + products_id);
        Log.d(TAG, "postPayment: " + products_name);
        Log.d(TAG, "postPayment: " + products_code);
        Log.d(TAG, "postPayment: " + quantity);
        params.setUseJsonStreamer(true);

        Log.d(TAG, "onPostPayment: " + requestUrl);

        client.post(requestUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d(TAG, "onSuccess: " + new String(responseBody));
                PaymentResponse memberResponse = gson.fromJson(new String(responseBody), PaymentResponse.class);
                if (memberResponse != null) {
                    callback.onSuccess();
                } else {
                    callback.onError("Cannot get Object", ErrorCode.NOT_FOUND);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onError(new String(responseBody), ErrorCode.NO_INTERNET);
            }
        });
    }

    public void postSuspend(int total, Customer customer, String note, String products_id, String products_name, String products_code, String quantity, String price, String cost,
                            final PaymentResponse.PaymentResponseCallback callback) {
        String requestUrl = BASE_URL + "insertsuspend";
        RequestParams params = new RequestParams();
        params.put("total", total);
        params.put("customer_id", customer.getId());
        params.put("customer_name", customer.getName());
        params.put("note", note);
        params.put("products_id", products_id);
        params.put("products_name", products_name);
        params.put("products_code", products_code);
        params.put("price", price);
        params.put("quantity", quantity);
        params.put("cost", cost);
        Log.d(TAG, "postPayment: " + products_id);
        Log.d(TAG, "postPayment: " + products_name);
        Log.d(TAG, "postPayment: " + products_code);
        Log.d(TAG, "postPayment: " + quantity);
        params.setUseJsonStreamer(true);

        Log.d(TAG, "onPostPayment: " + requestUrl);

        client.post(requestUrl, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.d(TAG, "onSuccess: " + new String(responseBody));
                PaymentResponse memberResponse = gson.fromJson(new String(responseBody), PaymentResponse.class);
                if (memberResponse != null) {
                    callback.onSuccess();
                } else {
                    callback.onError("Cannot get Object", ErrorCode.NOT_FOUND);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                callback.onError(new String(responseBody), ErrorCode.NO_INTERNET);
            }
        });
    }


    public void cancelAllReq() {
        client.cancelAllRequests(true);
    }

    public enum ErrorCode {
        NOT_FOUND,
        NO_INTERNET,
        SERVER_ERROR
    }


}

