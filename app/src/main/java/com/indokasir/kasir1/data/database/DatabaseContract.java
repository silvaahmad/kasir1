package com.indokasir.kasir1.data.database;

import android.provider.BaseColumns;

/**
 * Created by dicoding on 10/12/2017.
 */

public class DatabaseContract {

    static final class POS implements BaseColumns {
        static String TABLE_SALE = "sale";
        static String NAME = "name";
        static String QUANTITY = "qty";
        static String PRICE = "price";

    }
}
