package com.indokasir.kasir1.data;


import android.content.Context;

import com.indokasir.kasir1.data.local.SharedPrefHelper;
import com.indokasir.kasir1.data.model.customer.Customer;
import com.indokasir.kasir1.data.model.customer.CustomerResponse;
import com.indokasir.kasir1.data.model.login.LoginResponse;
import com.indokasir.kasir1.data.model.payment.PaymentResponse;
import com.indokasir.kasir1.data.model.product.ProductItem;
import com.indokasir.kasir1.data.model.product.ProductResponse;
import com.indokasir.kasir1.data.model.suspend.SuspendItem;
import com.indokasir.kasir1.data.model.suspend.SuspendResponse;
import com.indokasir.kasir1.data.network.APIRequest;

import java.util.ArrayList;
import java.util.List;

public class Repository {

    private Context mContext;
    private APIRequest apiRequest;
    private SharedPrefHelper prefs;
    private List<ProductItem> productItems;
    private SuspendItem suspendItem;
    private Customer customer;

    public SuspendItem getSuspendItem() {
        return suspendItem;
    }

    public void setSuspendItem(SuspendItem suspendItem) {
        this.suspendItem = suspendItem;
    }


    public Repository(Context mContext) {
        this.mContext = mContext;
        apiRequest = new APIRequest();
        prefs = new SharedPrefHelper(mContext);
        productItems = new ArrayList<>();
        customer = new Customer();

    }

    public void saveUser(String id, String email, String username) {
        prefs.putString("id", id);
        prefs.putString("email", email);
        prefs.putString("username", username);
    }

    public String getUserId() {
        return prefs.getString("id");
    }

    public String getUserEmail() {
        return prefs.getString("email");
    }

    public String getUserName() {
        return prefs.getString("username");
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public void getProducts(ProductResponse.ProductsResponseCallback callback) {
        apiRequest.getProducts(callback);
    }

    public void getSuspend(SuspendResponse.SuspendResponseCallback callback) {
        apiRequest.getSuspend(callback);
    }

    public void getLogin(String email, String password, LoginResponse.ResponseCallback callback) {
        apiRequest.getLogin(email, password, callback);
    }

    public List<ProductItem> getProductItems() {
        return productItems;
    }

    public void setProductItems(List<ProductItem> productItems) {
        this.productItems = productItems;
    }

    public void postPayment(int total, Customer customer, String products_id, String products_name, String products_code, String quantity, String price, String cost, PaymentResponse.PaymentResponseCallback callback) {
        apiRequest.postPayment(total, customer, products_id, products_name, products_code, quantity, price, cost, callback);
    }

    public void postSuspend(int total, Customer customer, String note, String products_id, String products_name, String products_code, String quantity, String price, String cost, PaymentResponse.PaymentResponseCallback callback) {
        apiRequest.postSuspend(total, customer, note, products_id, products_name, products_code, quantity, price, cost, callback);
    }

    public void getCustomer(CustomerResponse.CustomerResponseCallback callback) {
        apiRequest.getCustomer(callback);
    }
}
