package com.indokasir.kasir1.data.model.product;

import com.google.gson.annotations.SerializedName;
import com.indokasir.kasir1.data.network.APIRequest;

import java.util.List;

/**
 * Created by User on 3/4/2019.
 */

public class ProductResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("values")
    private List<ProductItem> productItems;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ProductItem> getProductItems() {
        return productItems;
    }

    public void setProductItems(List<ProductItem> productItems) {
        this.productItems = productItems;
    }

    public interface ProductsResponseCallback {
        void onSuccess(ProductResponse productResponse);

        void onError(String message, APIRequest.ErrorCode errorCode);
    }
}
