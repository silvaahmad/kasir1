package com.indokasir.kasir1.data.model.customer;

import com.google.gson.annotations.SerializedName;
import com.indokasir.kasir1.data.network.APIRequest;

import java.util.List;

/**
 * Created by User on 3/6/2019.
 */

public class CustomerResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("values")
    private List<Customer> customers;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public interface CustomerResponseCallback {
        void onSuccess(CustomerResponse customerResponse);

        void onError(String message, APIRequest.ErrorCode errorCode);
    }
}
