package com.indokasir.kasir1.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.indokasir.kasir1.data.model.product.ProductItem;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.indokasir.kasir1.data.database.DatabaseContract.POS.NAME;
import static com.indokasir.kasir1.data.database.DatabaseContract.POS.PRICE;
import static com.indokasir.kasir1.data.database.DatabaseContract.POS.QUANTITY;
import static com.indokasir.kasir1.data.database.DatabaseContract.POS.TABLE_SALE;


/**
 * Created by silva on 11/23/16.
 */

public class ProductHelper {
    private static String DATABASE_TABLE = TABLE_SALE;
    private Context context;
    private DatabaseHelper dataBaseHelper;
    private SQLiteDatabase database;

    public ProductHelper(Context context) {
        this.context = context;
    }

    public ProductHelper open() throws SQLException {
        dataBaseHelper = new DatabaseHelper(context);
        database = dataBaseHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dataBaseHelper.close();
    }

    /**
     * Gunakan method ini untuk ambil semua ProductItem yang ada
     * Otomatis di parsing ke dalam model ProductItem
     *
     * @return hasil query berbentuk array model ProductItem
     */
    public ArrayList<ProductItem> query() {
        ArrayList<ProductItem> arrayList = new ArrayList<ProductItem>();
        Cursor cursor = database.query(DATABASE_TABLE, new String[]{NAME, "sum(" + QUANTITY + ") as " + QUANTITY, PRICE}, null, null, NAME, null, _ID + " DESC", null);
        cursor.moveToFirst();
        ProductItem productItem;
        if (cursor.getCount() > 0) {
            do {

                productItem = new ProductItem();
                productItem.setName(cursor.getString(cursor.getColumnIndexOrThrow(NAME)));
                productItem.setQty(cursor.getInt(cursor.getColumnIndexOrThrow(QUANTITY)));
                productItem.setPrice(cursor.getInt(cursor.getColumnIndexOrThrow(PRICE)));

                arrayList.add(productItem);
                cursor.moveToNext();

            } while (!cursor.isAfterLast());
        }
        cursor.close();
        return arrayList;
    }

    /**
     * Gunakan method ini untuk query insert
     *
     * @param productItem model productItem yang akan dimasukkan
     * @return id dari data yang baru saja dimasukkan
     */
    public long insert(ProductItem productItem) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(NAME, productItem.getName());
        initialValues.put(QUANTITY, 1);
        initialValues.put(PRICE, productItem.getPrice());
        return database.insert(DATABASE_TABLE, null, initialValues);
    }

    /**
     * Gunakan method ini untuk query update
     *
     * @param productItem yang akan diubah
     * @return int jumlah dari row yang ter-update, jika tidak ada yang diupdate maka nilainya 0
     */
    public int update(ProductItem productItem) {
        ContentValues args = new ContentValues();
        args.put(NAME, productItem.getName());
        args.put(PRICE, productItem.getPrice());
        return database.update(DATABASE_TABLE, args, NAME + "= '" + productItem.getName() + "'", null);
    }

    /**
     * Gunakan method ini untuk query delete
     *
     * @param name yang akan di delete
     * @return int jumlah row yang di delete
     */
    public int delete(String name) {
        return database.delete(TABLE_SALE, NAME + " = '" + name + "'", null);
    }

}
