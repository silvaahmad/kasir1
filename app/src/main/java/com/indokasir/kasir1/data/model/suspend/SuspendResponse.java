package com.indokasir.kasir1.data.model.suspend;

import com.google.gson.annotations.SerializedName;
import com.indokasir.kasir1.data.network.APIRequest;

import java.util.List;

/**
 * Created by User on 3/4/2019.
 */

public class SuspendResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("values")
    private List<SuspendItem> suspendItems;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SuspendItem> getSuspendItems() {
        return suspendItems;
    }

    public void setSuspendItems(List<SuspendItem> suspendItems) {
        this.suspendItems = suspendItems;
    }

    public interface SuspendResponseCallback {
        void onSuccess(SuspendResponse SuspendResponse);

        void onError(String message, APIRequest.ErrorCode errorCode);
    }
}
