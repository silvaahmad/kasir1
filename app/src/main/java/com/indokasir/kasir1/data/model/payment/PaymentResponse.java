package com.indokasir.kasir1.data.model.payment;

import com.indokasir.kasir1.data.network.APIRequest;

/**
 * Created by User on 3/5/2019.
 */

public class PaymentResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public interface PaymentResponseCallback {
        void onSuccess();

        void onError(String message, APIRequest.ErrorCode errorCode);
    }
}
