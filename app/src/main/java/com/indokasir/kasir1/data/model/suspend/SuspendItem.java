package com.indokasir.kasir1.data.model.suspend;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SuspendItem implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("customer_name")
    @Expose
    private String customer_name;
    @SerializedName("hold_ref")
    @Expose
    private String hold_ref;
    @SerializedName("total_items")
    @Expose
    private int total_items;
    @SerializedName("grand_total")
    @Expose
    private int grand_total;
    @SerializedName("items")
    @Expose
    private List<Items> items;


    public SuspendItem() {
    }

    protected SuspendItem(Parcel in) {
        this.id = in.readInt();
    }

    public static final Creator<SuspendItem> CREATOR = new Creator<SuspendItem>() {
        @Override
        public SuspendItem createFromParcel(Parcel in) {
            return new SuspendItem(in);
        }

        @Override
        public SuspendItem[] newArray(int size) {
            return new SuspendItem[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getHold_ref() {
        return hold_ref;
    }

    public void setHold_ref(String hold_ref) {
        this.hold_ref = hold_ref;
    }

    public int getTotal_items() {
        return total_items;
    }

    public void setTotal_items(int total_items) {
        this.total_items = total_items;
    }

    public int getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(int grand_total) {
        this.grand_total = grand_total;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(this.id);
    }

}
