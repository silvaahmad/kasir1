package com.indokasir.kasir1.data.model.login;

import com.google.gson.annotations.SerializedName;
import com.indokasir.kasir1.data.network.APIRequest;

/**
 * Created by User on 3/5/2019.
 */

public class LoginResponse {
    @SerializedName("email")
    private String email;
    @SerializedName("username")
    private String username;
    @SerializedName("id")
    private String id;

    public interface ResponseCallback {
        void onSuccess(String id, String email, String username);

        void onError(String message, APIRequest.ErrorCode errorCode);
    }
}
